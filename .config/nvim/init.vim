syntax on
set number
set ignorecase
set smartcase
set cursorline

set wildmode=longest,list,full
noremap S :%s//gI<Left><Left><Left>

call plug#begin()
	Plug 'kovetskiy/sxhkd-vim'
	Plug 'ap/vim-css-color'
	Plug 'alvan/vim-closetag'
	Plug 'sheerun/vim-polyglot'
	Plug 'jiangmiao/auto-pairs'
call plug#end()


highlight LineNr	ctermfg=8    ctermbg=none    cterm=none
highlight CursorLineNr	ctermfg=7    ctermbg=8       cterm=none
highlight CursorLine	ctermfg=none ctermbg=8       cterm=none
highlight Statement	ctermfg=4    ctermbg=none    cterm=bold
highlight Directory	ctermfg=4    ctermbg=none    cterm=none
highlight Comment	ctermfg=14   ctermbg=none    cterm=italic
highlight Operator	ctermfg=4    ctermbg=none    cterm=none
highlight Constant	ctermfg=12   ctermbg=none    cterm=none
highlight Special	ctermfg=4    ctermbg=none    cterm=none
highlight Identifier	ctermfg=7    ctermbg=none    cterm=none
highlight PreProc	ctermfg=5    ctermbg=none    cterm=none
highlight String	ctermfg=2    ctermbg=none    cterm=none
highlight Number	ctermfg=5    ctermbg=none    cterm=none
highlight Function	ctermfg=1    ctermbg=none    cterm=none

" ------Vim Auto Closetag------
" filenames like *.xml, *.html, *.xhtml, ...
" These are the file extensions where this plugin is enabled.
let g:closetag_filenames = '*.html,*.xhtml,*.jsx,*.js,*.tsx'

" filenames like *.xml, *.xhtml, ...
" This will make the list of non-closing tags self-closing in the specified files.
let g:closetag_xhtml_filenames = '*.xml,*.xhtml,*.jsx,*.js,*.tsx'

" filetypes like xml, html, xhtml, ...
" These are the file types where this plugin is enabled.
let g:closetag_filetypes = 'html,xhtml,jsx,js,tsx'

" filetypes like xml, xhtml, ...
" This will make the list of non-closing tags self-closing in the specified files.
let g:closetag_xhtml_filetypes = 'xml,xhtml,jsx,js,tsx'

" integer value [0|1]
" This will make the list of non-closing tags case-sensitive (e.g. `<Link>` will be closed while `<link>` won't.)
let g:closetag_emptyTags_caseSensitive = 1

" Disables auto-close if not in a "valid" region (based on filetype)
let g:closetag_regions = {
    \ 'typescript.tsx': 'jsxRegion,tsxRegion',
    \ 'javascript.jsx': 'jsxRegion',
    \ }

" Shortcut for closing tags, default is '>'
let g:closetag_shortcut = '>'

" Add > at current position without closing the current tag, default is ''
let g:closetag_close_shortcut = '<leader>>'
